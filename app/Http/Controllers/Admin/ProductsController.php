<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Forms\ProductForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(10);
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Adicionar Produto";
        $form = \FormBuilder::create(ProductForm::class, [
          'method' => 'POST',
          'url' => route('admin.products.store')
        ]);
        return view('admin.products.save', compact('title', 'form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormBuilder $form_builder)
    {
        $form = $form_builder->create(ProductForm::class);

        Product::create($form->getFieldValues());

        return redirect()->route('admin.products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $title = "Atualizar Produto";
        $form = \FormBuilder::create(ProductForm::class, [
          'method' => 'PUT',
          'url' => route('admin.products.update', ['id' => $product->id]),
          'model' => $product
        ]);
        return view('admin.products.save', compact('title', 'form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(FormBuilder $form_builder, Product $product)
    {
      $form = $form_builder->create(ProductForm::class);
      $product->fill($form->getFieldValues());
      $product->save();

      return redirect()->route('admin.products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('admin.products.index');
    }
}
